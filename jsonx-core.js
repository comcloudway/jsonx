export const jsonx_core = (
  json = {},
  {
    // callback function used to create element from tag, props and children
    create_element = (tag, props, children)=>undefined,
  }
) => {

  let self = (json = {}) => {

    switch (typeof json) {
      case "object":
        if (Array.isArray(json)) {
          return json.map(entry=>self(entry));
        } else {
          if (
            Object.hasOwn(json, 'tag') &&
              ['string', 'function'].includes(typeof json.tag) &&
              !Object.hasOwn(json, '__nojsonx')
          ) {
            let {
              tag,
              children = undefined,
              content = undefined,
              ...props
            } = json;

            let builder = create_element;
            /*/if (typeof tag === 'function') {
              // use function to create element
              builder = tag;
              tag = undefined;
            }*/

            return builder(
              tag,
              self(props),
              self(children||content)
            );

          } else {
            return json;
          }
        }
        break;
      default:
        return json
    }

  }

  return self(json);
}
export default jsonx_core;

import jsonx_core from './jsonx-core.js';

/*
 * Converts a json object to html elements
 * also supports tree conversion
 *
 * JSON Object structure
 * {tag: string, children: array, ...props}
 *
 * @param {Object|any} json - the object/value to be converted into a html element
 * @returns {HTMLElement|any}
 */
export const jsonx = (json = {}) => jsonx_core(json, {
  create_element: (tag, props, children) => {
    let element = document.createElement(tag);

    let style = props.style;
    if (!!style) {
      Object.entries(style)
            .forEach(([key, value])=>element.style[key]=value);
    }
    delete props.style;

    if (!!children) {
      if (typeof children == 'object') {
        if (Array.isArray(children)) {
          element.append(...children);
        } else {
          element.children = children;
        }
      }  else {
        element.innerText = children.toString();
      }
    }

    if (!!props) {
      Object.entries(props)
            .forEach(([key, value]) => element[key.toLowerCase()]=value);
    }

    return element;
  }
});
export default jsonx;

import jsonx_core from './jsonx-core.js';
let self = import.meta;
let url = new URL(self.url);

let { h } = await import(url.searchParams.get("preact"));
/*
 * Converts a json object to preact elements
 * also supports tree conversion
 * use preacts render function to render the element
 *
 * JSON Object structure
 * {tag: string, children: array, ...props}
 *
 * @param {Object|any} json - the object/value to be converted into a preact element
 * @returns {Component|any}
 */
export const jsonx = (json = {}) => jsonx_core(json, {
  create_element: h
});
export default jsonx;
